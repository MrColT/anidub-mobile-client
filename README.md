# anidub-mobile-client #

Not the official Android Client for the site AniDub.
**I recommend** using the API from [this](https://github.com/MrCo1T/anidub-api) repository in the place with this client. It has all the necessary functions for this

### Features ###
* load anime list from [API](https://github.com/MrCo1T/anidub-api)
* display anime info

### Learn More ###

* Project: AniDub Android Client
* Version: 0.1 [Alpha]
* Java: 8 (recomended)
* Android SDK: 25+ (recomended)

### How to use ###

* Download/Clone files from this repository
* Open the project in any IDE for the Android application (I recomended to use 'Android Studio')
* Enjoy

### Requirements for the Contributors ###

* Thoroughly test the code
* More details about the innovations
* To work for the good of the people :D

### Where can I find the author? ###

* VK: https://vk.com/mrco1t
* E-Mail: coltredstone@gmail.com
